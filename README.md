ideaplexus/mysql
================

This repository contains a preconfigured docker container of mysql with `utf8mb4` character set and `utf8mb4_unicode_ci` collation.